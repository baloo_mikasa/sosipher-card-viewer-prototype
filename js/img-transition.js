jQuery(document).ready(function($) {

	// global variables
	var newImgWrapper = $(".char-b-img-wrapper"),
		newTypeWrapper = $(".balance-wrapper"),
		oldImgWrapper = '',
		oldTypeWrapper = '';

	// cache DOM elements
	var viewport = $(window);
		balance = $(".balance"),
		attack = $(".attack"),
		hp = $(".hp"),
		defense = $(".defense"),
		iStarDiv = $(".char-star-i"),
		fStarDiv = $(".char-star-f"),
		iStarSingle = $(".char-star-i i"),
		fStarSingle = $(".char-star-f i"),
		statButton = $(".char-stat-change-button"),
		statButtonMaxDiv = $(".char-stat-change-button .max"),
		statButtonMinDiv = $(".char-stat-change-button .min"),
		statButtonMaxSingle = $(".char-stat-change-button .max i"),
		statButtonMinSingle = $(".char-stat-change-button .min i");

	var x = "",
		y = "";

	if(viewport.width() >= 768) {
		x = "-400px";
		y = "-560px";
	}
	else {
		x = "-240px";
		y = "-336px";
	}

	viewport.resize(function() {
		if(viewport.width() >= 768) {
			x = "-400px";
			y = "-560px";
		}
		else {
			x = "-240px";
			y = "-336px";
		}
	});

	// type click response
	balance.click(function() {
		if( !(balance.hasClass("selected")) ) {
			var temp = getAndRemoveClass()
			oldImgWrapper = temp[0];
			oldTypeWrapper = temp[1];
			newImgWrapper = $(".char-b-img-wrapper");
			newTypeWrapper = $(".balance-wrapper");
			balance.addClass("selected");

			oldImgWrapper.css("animation","rotate-out 2s forwards");
			newImgWrapper.css("animation","rotate-in 2s forwards");

			oldTypeWrapper.css("animation","translate-left 1s forwards");
			newTypeWrapper.css("animation","translate-right 1s forwards");

			addAndRemoveClass(oldImgWrapper, newImgWrapper);
			restoreDefault(oldImgWrapper);
		}
	});
	attack.click(function() {
		if( !(attack.hasClass("selected")) ) {
			var temp = getAndRemoveClass()
			oldImgWrapper = temp[0];
			oldTypeWrapper = temp[1];
			newImgWrapper = $(".char-a-img-wrapper");
			newTypeWrapper = $(".attack-wrapper");
			attack.addClass("selected");

			oldImgWrapper.css("animation","rotate-out 2s forwards");
			newImgWrapper.css("animation","rotate-in 2s forwards");

			oldTypeWrapper.css("animation","translate-left 1s forwards");
			newTypeWrapper.css("animation","translate-right 1s forwards");

			addAndRemoveClass(oldImgWrapper, newImgWrapper);
			restoreDefault(oldImgWrapper);
		}
	});
	hp.click(function() {
		if( !(hp.hasClass("selected")) ) {
			var temp = getAndRemoveClass()
			oldImgWrapper = temp[0];
			oldTypeWrapper = temp[1];
			newImgWrapper = $(".char-h-img-wrapper");
			newTypeWrapper = $(".hp-wrapper");
			hp.addClass("selected");

			oldImgWrapper.css("animation","rotate-out 2s forwards");
			newImgWrapper.css("animation","rotate-in 2s forwards");

			oldTypeWrapper.css("animation","translate-left 1s forwards");
			newTypeWrapper.css("animation","translate-right 1s forwards");

			addAndRemoveClass(oldImgWrapper, newImgWrapper);
			restoreDefault(oldImgWrapper);
		}
	});
	defense.click(function() {
		if( !(defense.hasClass("selected")) ) {
			var temp = getAndRemoveClass()
			oldImgWrapper = temp[0];
			oldTypeWrapper = temp[1];
			newImgWrapper = $(".char-d-img-wrapper");
			newTypeWrapper = $(".defense-wrapper");
			defense.addClass("selected");

			oldImgWrapper.css("animation","rotate-out 2s forwards");
			newImgWrapper.css("animation","rotate-in 2s forwards");

			oldTypeWrapper.css("animation","translate-left 1s forwards");
			newTypeWrapper.css("animation","translate-right 1s forwards");

			addAndRemoveClass(oldImgWrapper, newImgWrapper);
			restoreDefault(oldImgWrapper);
		}
	});

	// initial star click response
	iStarDiv.click(function() {
		if( !(iStarDiv.hasClass("selected")) && !(statButton.hasClass("clicked")) ) {

			if(iStarDiv.hasClass("animated"))
				reactivateAnimation(iStarDiv);
			else
				animateStars(iStarSingle, iStarDiv);

			fStarDiv.removeClass("selected");
			iStarDiv.addClass("selected");

			newImgWrapper.css("animation","translate-horizontal-left-lvl1 1s");
			newImgWrapper.css("transform","translateX(0px)");
		}
		else if ( !(iStarDiv.hasClass("selected")) && statButton.hasClass("clicked") ) {
			var tempString = "translate(0px," + y + ")" ;

			if(iStarDiv.hasClass("animated")) 
				reactivateAnimation(iStarDiv);
			else 
				animateStars(iStarSingle, iStarDiv);

			fStarDiv.removeClass("selected");
			iStarDiv.addClass("selected");

			newImgWrapper.css("animation","translate-horizontal-left-lvlmax 1s");
			newImgWrapper.css("transform",tempString);
		}
	});
	// final star click response
	fStarDiv.click(function() {
		if( !(fStarDiv.hasClass("selected")) && !(statButton.hasClass("clicked")) ) {
			var tempString = "translateX(" + x + ")" ;

			if(fStarDiv.hasClass("animated"))
				reactivateAnimation(fStarDiv);
			else
				animateStars(fStarSingle, fStarDiv);

			iStarDiv.removeClass("selected");
			fStarDiv.addClass("selected");

			newImgWrapper.css("animation","translate-horizontal-right-lvl1 1s");
			newImgWrapper.css("transform",tempString);
		}
		else if ( !(fStarDiv.hasClass("selected")) && statButton.hasClass("clicked") ) {
			var tempString = "translate(" + x + "," + y + ")" ;

			if(fStarDiv.hasClass("animated"))
				reactivateAnimation(fStarDiv);
			else
				animateStars(fStarSingle, fStarDiv);

			iStarDiv.removeClass("selected");
			fStarDiv.addClass("selected");

			newImgWrapper.css("animation","translate-horizontal-right-lvlmax 1s");
			newImgWrapper.css("transform",tempString);
		}
	});

	// statButton click repsonses
	statButton.click(function() {
		if( !(statButton.hasClass("clicked")) && iStarDiv.hasClass("selected") ) {
			var tempString = "translateY(" + y + ")" ;

			statButton.addClass("clicked");

			newImgWrapper.css("animation","translate-vertical-up-fivestar 1s");
			newImgWrapper.css("transform",tempString);
		}
		else if( statButton.hasClass("clicked") && iStarDiv.hasClass("selected") ) {
			statButton.removeClass("clicked");

			newImgWrapper.css("animation","translate-vertical-down-fivestar 1s");
			newImgWrapper.css("transform","translateY(0px)");
		}
		else if( !(statButton.hasClass("clicked")) && !(iStarDiv.hasClass("selected")) ) {
			var tempString = "translate(" + x + "," + y + ")" ;

			statButton.addClass("clicked");

			newImgWrapper.css("animation","translate-vertical-up-redstar 1s");
			newImgWrapper.css("transform",tempString);
		}
		else if( statButton.hasClass("clicked") && !(iStarDiv.hasClass("selected")) ) {
			var tempString = "translate(" + x + ",0px)" ;

			statButton.removeClass("clicked");

			newImgWrapper.css("animation","translate-vertical-down-redstar 1s");
			newImgWrapper.css("transform",tempString);
		}
	});

	// re-animate
	function reactivateAnimation(tempClass) {
		var clone = tempClass.clone(true);

		tempClass.before(clone);
		tempClass.remove();

		updateValue();
	}

	// animating individual stars
	function animateStars(tempClassSingles, tempClassDiv) {
		tempClassSingles.css("animation-name","rotate");
		tempClassSingles.css("animation-duration","1s");
		tempClassSingles.css("animation-fill-mode","backwards");

		tempClassDiv.addClass("animated");
	}

	// get img-wrapper and remove selected class
	function getAndRemoveClass() {
		var tempImgClass = '';
		var tempTypeClass = '';

		if(balance.hasClass("selected")) {
			tempImgClass = $(".char-b-img-wrapper");
			tempTypeClass = $(".balance-wrapper");
			balance.removeClass("selected");
		}
		else if(attack.hasClass("selected")) {
			tempImgClass = $(".char-a-img-wrapper");
			tempTypeClass = $(".attack-wrapper");
			attack.removeClass("selected");
		}
		else if(hp.hasClass("selected")) {
			tempImgClass = $(".char-h-img-wrapper");
			tempTypeClass = $(".hp-wrapper");
			hp.removeClass("selected");
		}
		else if(defense.hasClass("selected")) {
			tempImgClass = $(".char-d-img-wrapper");
			tempTypeClass = $(".defense-wrapper");
			defense.removeClass("selected");
		}

		return [tempImgClass, tempTypeClass];
	}

	// adding and removing is-visible and is-hidden class
	function addAndRemoveClass(oldImgWrapper, newImgWrapper) {
		oldImgWrapper.removeClass("is-visible");
		oldImgWrapper.addClass("is-hidden");
		newImgWrapper.removeClass("is-hidden");
		newImgWrapper.addClass("is-visible");
	}

	// restoring to the inital lvl1 lowest star form with reanimation
	function restoreDefault(oldImgWrapper) {

		if(iStarDiv.hasClass("animated"))
			reactivateAnimation(iStarDiv);
		else
			animateStars(iStarSingle, iStarDiv);

		if(fStarDiv.hasClass("selected")) {
			fStarDiv.removeClass("selected");
			iStarDiv.addClass("selected");
		}

		if(statButton.hasClass("clicked")) {
			statButtonMinSingle.css("opacity","1");
			statButtonMinSingle.css("animation-name","scale-down");
			statButtonMinSingle.css("animation-duration",".6s");
			statButtonMinSingle.css("animation-fill-mode","forwards");

			statButtonMaxSingle.css("opacity","0");
			statButtonMaxSingle.css("animation-name","scale-up");
			statButtonMaxSingle.css("animation-duration",".6s");
			statButtonMaxSingle.css("animation-fill-mode","forwards");

			statButtonMinDiv.removeClass("visible");
			statButtonMaxDiv.addClass("visible");

			statButton.removeClass("clicked");
		}
		oldImgWrapper.css("transform","translate(0px,0px)");
	}

	// update value
	function updateValue() {
		iStarDiv = $(".char-star-i");
		fStarDiv = $(".char-star-f");
		iStarSingle = $(".char-star-i i");
		fStarSingle = $(".char-star-f i");
		statButton = $(".char-stat-change-button");
	}

});