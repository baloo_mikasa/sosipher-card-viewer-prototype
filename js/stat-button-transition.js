jQuery(document).ready(function($) {

	// cache DOM elements
	var statButtonMaxDiv = $(".char-stat-change-button .max"),
		statButtonMinDiv = $(".char-stat-change-button .min"),
		statButtonMaxSingle = $(".char-stat-change-button .max i"),
		statButtonMinSingle = $(".char-stat-change-button .min i");

	$(".char-stat-change-button").click(function () {
		if ( $(".max").hasClass("visible") ) {
			
			statButtonMaxSingle.css("opacity","1");
			statButtonMaxSingle.css("animation-name","scale-down");
			statButtonMaxSingle.css("animation-duration",".6s");
			statButtonMaxSingle.css("animation-fill-mode","forwards");

			statButtonMinSingle.css("opacity","0");
			statButtonMinSingle.css("animation-name","scale-up");
			statButtonMinSingle.css("animation-duration",".6s");
			statButtonMinSingle.css("animation-fill-mode","forwards");

			statButtonMaxDiv.removeClass("visible");
			statButtonMinDiv.addClass("visible");
		}
		else if ( !($(".char-stat-change-button").hasClass("clicked")) ) {
			
			statButtonMinSingle.css("opacity","1");
			statButtonMinSingle.css("animation-name","scale-down");
			statButtonMinSingle.css("animation-duration",".6s");
			statButtonMinSingle.css("animation-fill-mode","forwards");

			statButtonMaxSingle.css("opacity","0");
			statButtonMaxSingle.css("animation-name","scale-up");
			statButtonMaxSingle.css("animation-duration",".6s");
			statButtonMaxSingle.css("animation-fill-mode","forwards");

			statButtonMinDiv.removeClass("visible");
			statButtonMaxDiv.addClass("visible");
		}
	});
});