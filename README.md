# sosipher-card-viewer-prototype  #
## (소시퍼 카드뷰어) ##
### [See Prototype ONLINE!](http://sosiphercardviewerprototype-arteezy.rhcloud.com/index.html)###

Current Version : v1

( 현재버젼 : v1 )

__Third Party Files (외부 파일)__

* third party CSS files (외부 CSS 파일)
	* reset.css
	* bootstrap.css
* third party JS files (외부 JS 파일)
	* jquery-2.1.4.js
	* modernizr.js

__Other Specifications (기타 사양)__

* Image files that is in `img-wrapper > div` are content from Ntreev Soft CO. 
	* _I do not own any of these images_ 
* Other images (.svg files) are credited in the css by comments

__Contacts (연락처)__

<chungjinwoo5d@naver.com>